# Magento2实战 主题demo  ( 支持2.3.x - 2.4.x )

#### 项目介绍
Magento2实战教程的主题demo

#### 项目文档
[https://www.kancloud.cn/zouhongzhao/magento2-in-action/628441](https://www.kancloud.cn/zouhongzhao/magento2-in-action/628441)

#### 注意事项
如果你需要图片banner滚动的效果，就必须安装
https://gitee.com/zouhongzhao/m2-plug-zou-bannerslider
这个插件。

如果你不需要图片banner滚动效果，就注释掉
`app/design/frontend/Zou/demo/Magento_Cms/templates/html/homepage/home-bannerslider.phtml`里面的代码。
否则会报错。

#### 安装教程



1. 如果没有`app/design/frontend`目录的话就先创建`app/design/frontend`目录
1. 在`app/design/frontend`里创建`Zou`目录
1. 在`Zou`目录下 `git clone https://gitee.com/zouhongzhao/magento2-action-theme-demo.git .`
1. 主题路径为 `app/design/frontend/Zou/demo/theme.xml`
1. 后台启用主题
![启用主题](https://gitee.com/uploads/images/2018/0623/101006_55b741a6_1715291.png "启用主题.png")

